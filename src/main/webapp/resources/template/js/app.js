var Messenger = function(el,messages){
  'use strict';
  var m = this;
  
  m.init = function(){
    m.codeletters = "&#*+%?£@§$";
    m.message = 0;
    m.current_length = 0;
    m.fadeBuffer = false;
    m.messages = messages; 
    /*[
      'Login',
      'Sprites',
      'Niveles',
      'Game Coder'
    ];*/
    
    setTimeout(m.animateIn, 100);
  };
  
  m.generateRandomString = function(length){
    var random_text = '';
    while(random_text.length < length){
      random_text += m.codeletters.charAt(Math.floor(Math.random()*m.codeletters.length));
    } 
    
    return random_text;
  };
  
  m.animateIn = function(){
    if(m.current_length < m.messages[m.message].length){
      m.current_length = m.current_length + 2;
      if(m.current_length > m.messages[m.message].length) {
        m.current_length = m.messages[m.message].length;
      }
      
      var message = m.generateRandomString(m.current_length);
      $(el).html(message);
      
      setTimeout(m.animateIn, 20);
    } else { 
      setTimeout(m.animateFadeBuffer, 20);
    }
  };
  
  m.animateFadeBuffer = function(){
    if(m.fadeBuffer === false){
      m.fadeBuffer = [];
      for(var i = 0; i < m.messages[m.message].length; i++){
        m.fadeBuffer.push({c: (Math.floor(Math.random()*12))+1, l: m.messages[m.message].charAt(i)});
      }
    }
    
    var do_cycles = false;
    var message = ''; 
    
    for(var i = 0; i < m.fadeBuffer.length; i++){
      var fader = m.fadeBuffer[i];
      if(fader.c > 0){
        do_cycles = true;
        fader.c--;
        message += m.codeletters.charAt(Math.floor(Math.random()*m.codeletters.length));
      } else {
        message += fader.l;
      }
    }
    
    $(el).html(message);
    
    if(do_cycles === true){
      setTimeout(m.animateFadeBuffer, 50);
    } else {
      setTimeout(m.cycleText, 2000);
    }
  };
  
  m.cycleText = function(){
    m.message = m.message + 1;
    if(m.message >= m.messages.length){
      m.message = 0;
    }
    
    m.current_length = 0;
    m.fadeBuffer = false;
    $(el).html('');
    
    setTimeout(m.animateIn, 200);
  };
  
  m.init();
}

console.clear();
$(function(){
    var messages = [
      'Login',
      'Sprites',
      'Niveles',
      'Game Coder'
    ];
    var messenger = new Messenger($('#messenger'),messages);
});

function clear_All(command){
    var isValid = false;
    if(command === 'clear_all();'){
        isValid = true;
    }
    return isValid;
}

function clear_last(command){
    var isValid = false;
    if(command === 'clear_last();'){
        isValid = true;
    }
    return isValid;
}


//Validar Comando
 function isValidCommand(command,Array){
    var isValid = false;
    //Se verifica que los comandos sean validos 
    //Hacemos split por parentesis izquierdo
    var parentesisIzq = command.split("(");
    console.log('Parentesis Izq',parentesisIzq);
    if(parentesisIzq.length === 2 && (parentesisIzq[0] === 'move_left' ||
            parentesisIzq[0] === 'move_right' || parentesisIzq[0] === 'move_down'
            || parentesisIzq[0] === 'move_up')){
        //Si split nos da un arreglo de 2 elementos y si el elemento 0 
        //es una instruccion valida entonces validamos el elemento 1
        var parentesisDer = parentesisIzq[1].split(")");
        //Si split nos da un arreglo de un elemento y si el elemento 0 
        //es un numero entonces devolvemos que es un comando valido
        //es una cadena vacia se toma como movimiento normal
        if(parentesisDer.length === 2 && ($.isNumeric(parentesisDer[0]) ||
                parentesisDer[0] === "")
                && parentesisDer[1] === ";"){
            //Es una instruccion valida
            isValid = true;
            if($.isNumeric(parentesisDer[0])){
               var i = moveLevel[lvl] - parentesisDer[0];
               if(i >= 0){
                    Array.push(parentesisIzq[0]+"-"+parentesisDer[0]);
                    moveLevel[lvl] -= parentesisDer[0];
                    if(moveLevel[lvl] === 0){
                        movesTerm = -1;
                    }
               }else if(movesTerm !== -1){
                   movesTerm = 1;
               }
            }else{
               var i = moveLevel[lvl] - 1;
               if(i >= 0){
                   Array.push(parentesisIzq[0]+"-"+1);
                   moveLevel[lvl] -= 1;
                   if(moveLevel[lvl] === 0){
                       movesTerm = -1;
                   }
               }else if(movesTerm !== -1){
                   movesTerm = 1;
               }
            }
        }
    }

    return isValid;
}
