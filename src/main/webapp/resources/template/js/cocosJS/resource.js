
var base = $("#base").attr("base-attr");

var BackGround_png = base+"/resources/template/img/Sprites/background.png";
var Start_N_png = base+"/resources/template/img/Sprites/start_N.png";
var Start_S_png = base+"/resources/template/img/Sprites/start_S.png";
var Sushi_png = base+"/resources/template/img/Sprites/sushi_1n/sushi_1n.png";
var Sushi_plist  = base+"/resources/template/img/Sprites/sushi.plist";
var sushi_uno= base+"/resources/template/img/Sprites/Player.png";
var Sushi_png_2 = base+"/resources/template/img/Sprites/player_2.png";
var particle = base+"/resources/template/img/Sprites/wall.png";
var place = base+"/resources/template/img/Sprites/Planet_1.png";
var mario_plist = base+"/resources/template/img/Sprites/Mario.plist";
var mario = base+"/resources/template/img/Sprites/Mario.png";
var mario_stand = base+"/resources/template/img/Sprites/mario-walk-2.png";
var mario_stand_left = base+"/resources/template/img/Sprites/mario-walk-8.png";
var mario_stand_up = base+"/resources/template/img/Sprites/mario-walk-5.png";
var star_crate_plist = base+"/resources/template/img/Sprites/star_crate.plist";
var star_crate = base+"/resources/template/img/Sprites/star_crate.png";
var drop_crate_1 = base+"/resources/template/img/Sprites/crate_drop_1.png";
var drop_crate_2 = base+"/resources/template/img/Sprites/crate_drop_2.png";
var mario_win_plist = base+"/resources/template/img/Sprites/mario_win.plist";
var mario_win = base+"/resources/template/img/Sprites/mario_win.png";
var coins_plist = base+"/resources/template/img/Sprites/coins.plist";
var coins = base+"/resources/template/img/Sprites/coins.png";
var coins_taked_plist = base+"/resources/template/img/Sprites/coins_taked.plist";
var coins_taked = base+"/resources/template/img/Sprites/coins_taked.png";
var chrono_stand_plist = base+"/resources/template/img/Sprites/chrono_stand.plist";
var chrono_stand = base+"/resources/template/img/Sprites/chrono_stand.png";
var chrono_down_plist = base+"/resources/template/img/Sprites/chrono_down.plist";
var chrono_down = base+"/resources/template/img/Sprites/chrono_down.png";
var chrono_up_plist = base+"/resources/template/img/Sprites/chrono_up.plist";
var chrono_up = base+"/resources/template/img/Sprites/chrono_up.png";
var chrono_stand_up = base+"/resources/template/img/Sprites/chrono-stand-up.png";
var chrono_left_plist = base+"/resources/template/img/Sprites/chrono_left.plist";
var chrono_left = base+"/resources/template/img/Sprites/chrono_left.png";
var chrono_stand_left_plist = base+"/resources/template/img/Sprites/chrono_stand_left.plist";
var chrono_stand_left = base+"/resources/template/img/Sprites/chrono_stand_left.png";
var chrono_win_plist = base+"/resources/template/img/Sprites/chrono_win.plist";
var chrono_win = base+"/resources/template/img/Sprites/chrono_win.png";
var rock_down_plist = base+"/resources/template/img/Sprites/rock-down.plist";
var rock_down = base+"/resources/template/img/Sprites/rock-down.png";
var mario_over_plist = base+"/resources/template/img/Sprites/mario-over.plist";
var mario_over = base+"/resources/template/img/Sprites/mario-over.png";
var mario_over_up_plist = base+"/resources/template/img/Sprites/mario-over-up.plist";
var mario_over_up = base+"/resources/template/img/Sprites/mario-over-up.png";
var chrono_over_plist = base+"/resources/template/img/Sprites/chrono_over.plist";
var chrono_over = base+"/resources/template/img/Sprites/chrono_over.png";
var dkc_balloon_plist = base+"/resources/template/img/Sprites/dkc-balloon.plist";
var dkc_balloon = base+"/resources/template/img/Sprites/dkc-balloon.png";
var dkc_balloon_explode_plist = base+"/resources/template/img/Sprites/dkc-balloon-explode.plist";
var dkc_balloon_explode = base+"/resources/template/img/Sprites/dkc-balloon-explode.png";
var dkc_ball = base+"/resources/template/img/Sprites/dkc-balloon-1.png";
var mario_game_over_plist = base+"/resources/template/img/Sprites/mario-game-over.plist";
var mario_game_over = base+"/resources/template/img/Sprites/mario-game-over.png";
var chrono_game_over_plist = base+"/resources/template/img/Sprites/chrono-game-over.plist";
var chrono_game_over = base+"/resources/template/img/Sprites/chrono-game-over.png";
var g_ressources = [
	//image
    coins_taked_plist,
    coins_taked,
    Start_S_png,
    Sushi_png,
    Sushi_plist,
    Sushi_png_2,
    sushi_uno,
    particle,
    place,
    mario_plist, //9
    mario,       //10
    mario_stand, //11
    mario_stand_left, //12
    mario_stand_up, //13
    star_crate_plist, //14
    star_crate,      //15
    drop_crate_1,	 //16
    drop_crate_2,	 //17
    mario_win_plist, //18
    mario_win,       //19
    coins_plist,     //20
    coins,           //21 /* Chrono Trigger */ 
    chrono_stand_plist, //22
    chrono_stand,    //23
    chrono_down_plist,  //24
    chrono_down,        //25
    chrono_up_plist,    //26
    chrono_up,          //27
    chrono_stand_up,    //28
    chrono_left_plist,  //29
    chrono_left,        //30
    chrono_stand_left_plist, //31
    chrono_stand_left,   //32
    chrono_win_plist,    //33
    chrono_win,          //34
    rock_down_plist,     //35
    rock_down,           //36
    mario_over_plist,    //37
    mario_over,          //38
    mario_over_up_plist, //39
    mario_over_up,       //40
    chrono_over_plist,   //41
    chrono_over,         //42
    dkc_balloon_plist,   //43
    dkc_balloon,         //44
    dkc_balloon_explode_plist,  //45
    dkc_balloon_explode,    //46
    dkc_ball,                //47
    mario_game_over_plist,   //48
    mario_game_over,         //49
    chrono_game_over_plist,  //50
    chrono_game_over         //51
];
