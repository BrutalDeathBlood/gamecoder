/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.entity.classes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alan
 */
@Entity
@Table(name = "Niveles")
@NamedQueries({
    @NamedQuery(name = "Niveles.findAll", query = "SELECT n FROM Niveles n"),
    @NamedQuery(name = "Niveles.findByIdNiveles", query = "SELECT n FROM Niveles n WHERE n.idNiveles = :idNiveles"),
    @NamedQuery(name = "Niveles.findByNombre", query = "SELECT n FROM Niveles n WHERE n.nombre = :nombre"),
    @NamedQuery(name = "Niveles.findByPuntaje", query = "SELECT n FROM Niveles n WHERE n.puntaje = :puntaje"),
    @NamedQuery(name = "Niveles.findAllLevels", query = "SELECT n FROM Niveles n WHERE n.borrado=?1 "),
    @NamedQuery(name = "Niveles.findById", query = "SELECT n FROM Niveles n WHERE n.idNiveles=?1 ")
})
public class Niveles implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idNiveles")
    private Integer idNiveles;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "Puntaje")
    private BigDecimal puntaje;
    @Basic(optional = false)
    @NotNull
    @Column(name = "borrado")
    private int borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "nivelesidNiveles", fetch = FetchType.EAGER)
    private Collection<Objetos> objetosCollection;

    public Niveles() {
    }

    public Niveles(Integer idNiveles) {
        this.idNiveles = idNiveles;
    }

    public Niveles(Integer idNiveles, String nombre, BigDecimal puntaje, int borrado) {
        this.idNiveles = idNiveles;
        this.nombre = nombre;
        this.puntaje = puntaje;
        this.borrado = borrado;
    }

    public Integer getIdNiveles() {
        return idNiveles;
    }

    public void setIdNiveles(Integer idNiveles) {
        this.idNiveles = idNiveles;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(BigDecimal puntaje) {
        this.puntaje = puntaje;
    }

    public int getBorrado() {
        return borrado;
    }

    public void setBorrado(int borrado) {
        this.borrado = borrado;
    }

    public Collection<Objetos> getObjetosCollection() {
        return objetosCollection;
    }

    public void setObjetosCollection(Collection<Objetos> objetosCollection) {
        this.objetosCollection = objetosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNiveles != null ? idNiveles.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Niveles)) {
            return false;
        }
        Niveles other = (Niveles) object;
        if ((this.idNiveles == null && other.idNiveles != null) || (this.idNiveles != null && !this.idNiveles.equals(other.idNiveles))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gamecoder.cocos.game.entity.classes.Niveles[ idNiveles=" + idNiveles + " ]";
    }
    
}
