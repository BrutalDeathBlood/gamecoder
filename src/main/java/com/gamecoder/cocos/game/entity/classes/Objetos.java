/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.entity.classes;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alan
 */
@Entity
@Table(name = "Objetos")
@NamedQueries({
    @NamedQuery(name = "Objetos.findAll", query = "SELECT o FROM Objetos o"),
    @NamedQuery(name = "Objetos.findByIdObjetos", query = "SELECT o FROM Objetos o WHERE o.idObjetos = :idObjetos"),
    @NamedQuery(name = "Objetos.findByNombre", query = "SELECT o FROM Objetos o WHERE o.nombre = :nombre"),
    @NamedQuery(name = "Objetos.findByX", query = "SELECT o FROM Objetos o WHERE o.x = :x"),
    @NamedQuery(name = "Objetos.findByY", query = "SELECT o FROM Objetos o WHERE o.y = :y"),
    @NamedQuery(name = "Objetos.findByBorrado", query = "SELECT o FROM Objetos o WHERE o.borrado = :borrado"),
    @NamedQuery(name = "Objetos.findAllObjects", query = "SELECT o FROM Objetos o WHERE o.borrado=?1"),
    @NamedQuery(name = "Objetos.findById", query = "SELECT o FROM Objetos o WHERE o.idObjetos=?1")
})
public class Objetos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idObjetos")
    private Integer idObjetos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "X")
    private String x;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Y")
    private String y;
    @Basic(optional = false)
    @NotNull
    @Column(name = "borrado")
    private int borrado;
    @JoinColumn(name = "Assets_idAssets", referencedColumnName = "idAssets")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Assets assetsidAssets;
    @JoinColumn(name = "Niveles_idNiveles", referencedColumnName = "idNiveles")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Niveles nivelesidNiveles;

    public Objetos() {
    }

    public Objetos(Integer idObjetos) {
        this.idObjetos = idObjetos;
    }

    public Objetos(Integer idObjetos, String nombre, String x, String y, int borrado) {
        this.idObjetos = idObjetos;
        this.nombre = nombre;
        this.x = x;
        this.y = y;
        this.borrado = borrado;
    }

    public Integer getIdObjetos() {
        return idObjetos;
    }

    public void setIdObjetos(Integer idObjetos) {
        this.idObjetos = idObjetos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public int getBorrado() {
        return borrado;
    }

    public void setBorrado(int borrado) {
        this.borrado = borrado;
    }

    public Assets getAssetsidAssets() {
        return assetsidAssets;
    }

    public void setAssetsidAssets(Assets assetsidAssets) {
        this.assetsidAssets = assetsidAssets;
    }

    public Niveles getNivelesidNiveles() {
        return nivelesidNiveles;
    }

    public void setNivelesidNiveles(Niveles nivelesidNiveles) {
        this.nivelesidNiveles = nivelesidNiveles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idObjetos != null ? idObjetos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Objetos)) {
            return false;
        }
        Objetos other = (Objetos) object;
        if ((this.idObjetos == null && other.idObjetos != null) || (this.idObjetos != null && !this.idObjetos.equals(other.idObjetos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gamecoder.cocos.game.entity.classes.Objetos[ idObjetos=" + idObjetos + " ]";
    }
    
}
