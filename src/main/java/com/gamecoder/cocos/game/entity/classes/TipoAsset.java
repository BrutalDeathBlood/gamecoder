/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.entity.classes;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alan
 */
@Entity
@Table(name = "TipoAsset")
@NamedQueries({
    @NamedQuery(name = "TipoAsset.findAll", query = "SELECT t FROM TipoAsset t"),
    @NamedQuery(name = "TipoAsset.findByIdTipoAsset", query = "SELECT t FROM TipoAsset t WHERE t.idTipoAsset = :idTipoAsset"),
    @NamedQuery(name = "TipoAsset.findByTipo", query = "SELECT t FROM TipoAsset t WHERE t.tipo = :tipo"),
    @NamedQuery(name = "TipoAsset.findByBorrado", query = "SELECT t FROM TipoAsset t WHERE t.borrado = :borrado"),
    @NamedQuery(name = "TipoAsset.findByAssetId", query = "SELECT t FROM TipoAsset t WHERE t.borrado=?1 and t.assetsidAssets.idAssets=?2 ")
})
public class TipoAsset implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipoAsset")
    private Integer idTipoAsset;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Tipo")
    private String tipo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "borrado")
    private int borrado;
    @JoinColumn(name = "Assets_idAssets", referencedColumnName = "idAssets")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Assets assetsidAssets;

    public TipoAsset() {
    }

    public TipoAsset(Integer idTipoAsset) {
        this.idTipoAsset = idTipoAsset;
    }

    public TipoAsset(Integer idTipoAsset, String tipo, int borrado) {
        this.idTipoAsset = idTipoAsset;
        this.tipo = tipo;
        this.borrado = borrado;
    }

    public Integer getIdTipoAsset() {
        return idTipoAsset;
    }

    public void setIdTipoAsset(Integer idTipoAsset) {
        this.idTipoAsset = idTipoAsset;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getBorrado() {
        return borrado;
    }

    public void setBorrado(int borrado) {
        this.borrado = borrado;
    }

    public Assets getAssetsidAssets() {
        return assetsidAssets;
    }

    public void setAssetsidAssets(Assets assetsidAssets) {
        this.assetsidAssets = assetsidAssets;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoAsset != null ? idTipoAsset.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoAsset)) {
            return false;
        }
        TipoAsset other = (TipoAsset) object;
        if ((this.idTipoAsset == null && other.idTipoAsset != null) || (this.idTipoAsset != null && !this.idTipoAsset.equals(other.idTipoAsset))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gamecoder.cocos.game.entity.classes.TipoAsset[ idTipoAsset=" + idTipoAsset + " ]";
    }
    
}
