/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.entity.classes;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alan
 */
@Entity
@Table(name = "Assets")
@NamedQueries({
    @NamedQuery(name = "Assets.findAll", query = "SELECT a FROM Assets a"),
    @NamedQuery(name = "Assets.findByIdAssets", query = "SELECT a FROM Assets a WHERE a.idAssets = :idAssets"),
    @NamedQuery(name = "Assets.findByNombre", query = "SELECT a FROM Assets a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Assets.findByPath", query = "SELECT a FROM Assets a WHERE a.path = :path"),
    @NamedQuery(name = "Assets.findByBorrado", query = "SELECT a FROM Assets a WHERE a.borrado = :borrado"),
    @NamedQuery(name = "Assets.findAllAssets", query = "SELECT a FROM Assets a WHERE a.borrado=?1"),
    @NamedQuery(name = "Assets.findById", query = "SELECT a FROM Assets a WHERE a.borrado=?1 AND a.idAssets=?2"),
    @NamedQuery(name = "Assets.findByIdAll", query = "SELECT a FROM Assets a WHERE a.idAssets=?1")
})
public class Assets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAssets")
    private Integer idAssets;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "Path")
    private String path;
    @Basic(optional = false)
    @NotNull
    @Column(name = "borrado")
    private int borrado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assetsidAssets", fetch = FetchType.EAGER)
    private Collection<TipoAsset> tipoAssetCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assetsidAssets", fetch = FetchType.EAGER)
    private Collection<Objetos> objetosCollection;

    public Assets() {
    }

    public Assets(Integer idAssets) {
        this.idAssets = idAssets;
    }

    public Assets(Integer idAssets, String nombre, String path, int borrado) {
        this.idAssets = idAssets;
        this.nombre = nombre;
        this.path = path;
        this.borrado = borrado;
    }

    public Integer getIdAssets() {
        return idAssets;
    }

    public void setIdAssets(Integer idAssets) {
        this.idAssets = idAssets;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getBorrado() {
        return borrado;
    }

    public void setBorrado(int borrado) {
        this.borrado = borrado;
    }

    public Collection<TipoAsset> getTipoAssetCollection() {
        return tipoAssetCollection;
    }

    public void setTipoAssetCollection(Collection<TipoAsset> tipoAssetCollection) {
        this.tipoAssetCollection = tipoAssetCollection;
    }

    public Collection<Objetos> getObjetosCollection() {
        return objetosCollection;
    }

    public void setObjetosCollection(Collection<Objetos> objetosCollection) {
        this.objetosCollection = objetosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAssets != null ? idAssets.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assets)) {
            return false;
        }
        Assets other = (Assets) object;
        if ((this.idAssets == null && other.idAssets != null) || (this.idAssets != null && !this.idAssets.equals(other.idAssets))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gamecoder.cocos.game.entity.classes.Assets[ idAssets=" + idAssets + " ]";
    }
    
}
