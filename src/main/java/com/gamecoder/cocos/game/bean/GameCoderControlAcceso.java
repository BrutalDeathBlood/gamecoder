/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.bean;

import com.gamecoder.cocos.game.entity.classes.Usuarios;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author alan
 */
@ManagedBean
@ViewScoped
public class GameCoderControlAcceso implements Serializable{

    /**
     * Creates a new instance of GameCoderControlAcceso
     */
    public GameCoderControlAcceso() {
    }
    
    public void verificaSesion(){
        FacesContext contexto=FacesContext.getCurrentInstance();
        try {    
            Usuarios usuario=(Usuarios)contexto.getExternalContext().getSessionMap().get("usuario");
            /*if(usuario==null){
                contexto.getExternalContext().redirect("./../Public/main.gamecoder");
            }else{*/
               String pagina=contexto.getViewRoot().getViewId();
               String[] tokens=pagina.split("/");
               if(tokens[1].equals("Private") && tokens[2].equals("Usuarios")){
                    if(usuario == null){
                        //contexto.getExternalContext().redirect("./../Private/Landing.gamecoder");
                        contexto.getExternalContext().redirect("./../../Public/main.gamecoder");
                    }
               }
               if(tokens[1].equals("Private")){
                   if(usuario == null){
                        //contexto.getExternalContext().redirect("./../Private/Landing.gamecoder");
                        contexto.getExternalContext().redirect("./../Public/main.gamecoder");
                    }
               }
            //}
        } catch (Exception e) {
        }
    }
    
    /**
     * 
     * Metodo que cierra sesion
     * eliminando el objecto usuario del sessionMap
     * 
     */
    public void cerrarSesion(){
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().invalidateSession();
            contexto.getExternalContext().getSessionMap().remove("usuario");
            contexto.getExternalContext().redirect(contexto.getExternalContext().getRequestContextPath()+"/Admin/login.gamecoder");
        } catch (Exception e) {
        }
    }
    
    public int getUsuario(){
        FacesContext contexto=FacesContext.getCurrentInstance();
        Usuarios usuario= (Usuarios)contexto.getExternalContext().getSessionMap().get("usuario");
        System.out.println("GetRendered: "+(usuario != null ? usuario.getIdUsuarios() : -1));
        return usuario != null ? usuario.getIdUsuarios() : -1;
    }
    
}
