/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.bean;

import com.gamecoder.cocos.game.entity.classes.Assets;
import com.gamecoder.cocos.game.entity.classes.Niveles;
import com.gamecoder.cocos.game.entity.classes.Objetos;
import com.gamecoder.cocos.game.facades.AssetsFacadeLocal;
import com.gamecoder.cocos.game.facades.NivelesFacadeLocal;
import com.gamecoder.cocos.game.facades.ObjetosFacadeLocal;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author alan
 */
@ManagedBean
@ViewScoped
public class GameCoderObjectsBean implements Serializable{
    
    @EJB
    private ObjetosFacadeLocal servicio;
    @EJB
    private AssetsFacadeLocal servAssets;
    @EJB
    private NivelesFacadeLocal servLevel;
    private Objetos objectTemp;
    private Objetos object;
    private List<Objetos> listaObjetos;
    private List<Assets> listaAssets;
    private List<Niveles> listaNiveles;
    private Assets asset;
    private Niveles level;
    
    /**
     * Creates a new instance of GameCoderObjectsBean
     */
    public GameCoderObjectsBean() {
    }
 
    @PostConstruct
    public void init(){
        try{
           listaObjetos = servicio.findAllObjects();
           listaAssets = servAssets.findAllAssets();
           listaNiveles = servLevel.findAllLevels();
           object = new Objetos();
           objectTemp = (Objetos) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("objectEditar");
            
            String pagina=FacesContext.getCurrentInstance()
                        .getViewRoot()
                        .getViewId();
            if(objectTemp != null && !pagina.contains("ObjectsInsert")){
                System.out.println("Entra en editar");
                System.out.println("Object: "+objectTemp.getNombre());
                asset = objectTemp.getAssetsidAssets();
                level = objectTemp.getNivelesidNiveles();
            }else{
                 FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .remove("objectEditar");
            }
           
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public Objetos getObjectTemp() {
        return objectTemp;
    }

    public void setObjectTemp(Objetos objectTemp) {
        this.objectTemp = objectTemp;
    }

    public Objetos getObject() {
        return object;
    }

    public void setObject(Objetos object) {
        this.object = object;
    }

    public List<Objetos> getListaObjetos() {
        return listaObjetos;
    }

    public void setListaObjetos(List<Objetos> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    public List<Assets> getListaAssets() {
        return listaAssets;
    }

    public void setListaAssets(List<Assets> listaAssets) {
        this.listaAssets = listaAssets;
    }

    public List<Niveles> getListaNiveles() {
        return listaNiveles;
    }

    public void setListaNiveles(List<Niveles> listaNiveles) {
        this.listaNiveles = listaNiveles;
    }

    public Assets getAsset() {
        return asset;
    }

    public void setAsset(Assets asset) {
        this.asset = asset;
    }

    public Niveles getLevel() {
        return level;
    }

    public void setLevel(Niveles level) {
        this.level = level;
    }
    
    public void prepareForEdit(Objetos object){
        FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .put("objectEditar", object);
        System.out.println("Entra en prepare edit");
        System.out.println("Object select: "+object.getNombre());
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Objetos/ObjectsEdit.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderObjectsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void InsertObjectViewRedirect(){
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Objetos/ObjectsInsert.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderObjectsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String createObject(){
        String action = "";
        if(!object.getNombre().equals("")){
            if(object.getX().matches("[0-9]+") && object.getY().matches("[0-9]+")){
                //Insertamos el Nivel
                object.setBorrado(0);
                object.setAssetsidAssets(asset);
                object.setNivelesidNiveles(level);
                servicio.create(object);
                listaObjetos = servicio.findAllObjects();
                Collections.sort(listaObjetos, new Comparator<Objetos>(){
                    @Override
                    public int compare(Objetos o1, Objetos o2) {
                        return o1.getIdObjetos().compareTo(o2.getIdObjetos());
                    }
                });
                if(servicio.findById(listaObjetos.get(listaObjetos.size()-1).getIdObjetos()) != null){
                    action="/Private/Objects?faces-redirect=true";
                    FacesContext
                    .getCurrentInstance()
                    .addMessage("msg",new FacesMessage("Objeto guardado!"));
                }else{
                    FacesContext.getCurrentInstance()
                        .addMessage("msg",new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Hubo un error la Base de Datos!",
                                "Hubo un error la Base de Datos!"));
                }
            }else{
                FacesContext.getCurrentInstance()
                            .addMessage("msg",new FacesMessage(
                                    FacesMessage.SEVERITY_ERROR,
                                    "Las coordenadas en X/Y deben ser numericas!",
                                    "Las  coordenadas en X/Y deben ser numericas!"));
            }
        }
        return action;
    }
    
    public String editObject(){
        String action = "";
        if(!objectTemp.getNombre().equals("")){
            if(objectTemp.getX().matches("[0-9]+") && objectTemp.getY().matches("[0-9]+")){
                //Insertamos el Nivel
                if(!asset.equals(objectTemp.getAssetsidAssets())){
                   objectTemp.setAssetsidAssets(asset);
                }
                if(!level.equals(objectTemp.getNivelesidNiveles())){
                    objectTemp.setNivelesidNiveles(level); 
                }
                servicio.edit(objectTemp);
                listaObjetos = servicio.findAllObjects();
                Collections.sort(listaObjetos, new Comparator<Objetos>(){
                    @Override
                    public int compare(Objetos o1, Objetos o2) {
                        return o1.getIdObjetos().compareTo(o2.getIdObjetos());
                    }
                });
                if(servicio.findById(listaObjetos.get(listaObjetos.size()-1).getIdObjetos()) != null){
                    action="/Private/Objects?faces-redirect=true";
                    FacesContext
                    .getCurrentInstance()
                    .addMessage("msg",new FacesMessage("Objeto editado!"));
                }else{
                    FacesContext.getCurrentInstance()
                        .addMessage("msg",new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Hubo un error la Base de Datos!",
                                "Hubo un error la Base de Datos!"));
                }
            }else{
                FacesContext.getCurrentInstance()
                            .addMessage("msg",new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Las coordenadas en X/Y deben ser numericas!",
                                "Las  coordenadas en X/Y deben ser numericas!"));
            }
        }
        return action;
    }
    
    public void deleteObject(Objetos objectAux){
        try {
            //servicio.remove(usuarioAux);
            objectAux.setBorrado(1);
            servicio.edit(objectAux);
            if(servicio.findById(objectAux.getIdObjetos()).getBorrado() == 1){
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Objeto Eliminado!"));
                object=new Objetos();
                objectTemp=new Objetos();
                listaObjetos = servicio.findAllObjects();
            }else{
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema al eliminar el objeto!"));
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema en la Base de Datos!"));
        }
    }
    
}
