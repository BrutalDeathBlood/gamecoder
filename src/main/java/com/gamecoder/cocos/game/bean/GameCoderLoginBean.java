/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.bean;

import com.gamecoder.cocos.game.entity.classes.Usuarios;
import com.gamecoder.cocos.game.facades.UsuariosFacadeLocal;
import com.gamecoder.cocos.game.util.CommonsUtilsGameCoder;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author alan
 */
@ManagedBean
@ViewScoped
public class GameCoderLoginBean implements Serializable{

    @EJB
    private UsuariosFacadeLocal servicio;
    private Usuarios usuario;
    
    
    /**
     * Creates a new instance of GameCoderLoginBean
     */
    public GameCoderLoginBean() {
    }
    
    @PostConstruct
    public void inicio(){
        usuario=new Usuarios();
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }
    
    public String credenciales(){
        Usuarios usuarioTemp=null;
        String cadena=null;
        try {
            usuario.setPassword(CommonsUtilsGameCoder.sha256DigestHash(usuario.getPassword()));
            System.out.println("Hash: "+CommonsUtilsGameCoder.sha256DigestHash(usuario.getPassword()));
            usuarioTemp=servicio.inicioSesion(usuario);
            if(usuarioTemp!=null){
                FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .put("usuario", usuarioTemp);
                cadena="/Private/Landing?faces-redirect=true";
            }else{
                FacesContext.getCurrentInstance().addMessage("mensaje",
                        new FacesMessage("Credenciales no validas"));
                usuario=new Usuarios();
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("mensaje",
                        new FacesMessage("Error en la bd"));
        }
        return cadena;
    }
}
