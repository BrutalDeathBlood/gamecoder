/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.bean;

import com.gamecoder.cocos.game.entity.classes.Niveles;
import com.gamecoder.cocos.game.facades.NivelesFacadeLocal;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author alan
 */
@ManagedBean
@ViewScoped
public class GameCoderLevelsBean implements Serializable{

    @EJB
    private NivelesFacadeLocal servicio;
    private Niveles levelTemp;
    private Niveles level;
    private List<Niveles> listaNiveles;
    private String pointsLevel;
    
    /**
     * Creates a new instance of GameCoderLevelsBean
     */
    public GameCoderLevelsBean() {
    }
    
    
    @PostConstruct
    public void init(){
        try{
            listaNiveles = servicio.findAllLevels();
            level = new Niveles();
            levelTemp = (Niveles) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("levelEditar");
            
            String pagina=FacesContext.getCurrentInstance()
                        .getViewRoot()
                        .getViewId();
            if(levelTemp != null && !pagina.contains("LevelsInsert")){
                System.out.println("Entra en editar");
                System.out.println("Nivel: "+levelTemp.getNombre());
                pointsLevel = levelTemp.getPuntaje().toPlainString().replace(".00","");
            }else{
                 FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .remove("levelEditar");
            }
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public Niveles getLevelTemp() {
        return levelTemp;
    }

    public void setLevelTemp(Niveles levelTemp) {
        this.levelTemp = levelTemp;
    }

    public List<Niveles> getListaNiveles() {
        return listaNiveles;
    }

    public void setListaNiveles(List<Niveles> listaNiveles) {
        this.listaNiveles = listaNiveles;
    }

    public Niveles getLevel() {
        return level;
    }

    public void setLevel(Niveles level) {
        this.level = level;
    }

    public String getPointsLevel() {
        return pointsLevel;
    }

    public void setPointsLevel(String pointsLevel) {
        this.pointsLevel = pointsLevel;
    }
    
    public void prepareForEdit(Niveles level){
        FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .put("levelEditar", level);
        System.out.println("Entra en prepare edit");
        System.out.println("Level select: "+level.getNombre());
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Niveles/LevelsEdit.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderLevelsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    public void InsertLevelViewRedirect(){
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Niveles/LevelsInsert.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderAssetsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String createLevel(){
        String action = "";
        pointsLevel = pointsLevel.trim().replace(" ","");
        if(!level.getNombre().equals("")){
            if(pointsLevel.matches("[0-9]+")){
                //Insertamos el Nivel
                level.setBorrado(0);
                level.setPuntaje(new BigDecimal(pointsLevel));
                servicio.create(level);
                listaNiveles = servicio.findAllLevels();
                Collections.sort(listaNiveles, new Comparator<Niveles>(){
                    @Override
                    public int compare(Niveles o1, Niveles o2) {
                        return o1.getIdNiveles().compareTo(o2.getIdNiveles());
                    }
                });
                if(servicio.findById(listaNiveles.get(listaNiveles.size()-1).getIdNiveles()) != null){
                    action="/Private/Levels?faces-redirect=true";
                    FacesContext
                    .getCurrentInstance()
                    .addMessage("msg",new FacesMessage("Nivel guardado!"));
                }else{
                    FacesContext.getCurrentInstance()
                        .addMessage("msg",new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Hubo un error la Base de Datos!",
                                "Hubo un error la Base de Datos!"));
                }
            }else{
                FacesContext.getCurrentInstance()
                            .addMessage("msg",new FacesMessage(
                                    FacesMessage.SEVERITY_ERROR,
                                    "El puntaje debe ser numerico!",
                                    "EL puntaje debe ser numerico!"));
            }
        }
        return action;
    }
    
    public String editLevel(){
        String action = "";
        pointsLevel = pointsLevel.trim().replace(" ","");
        if(!level.getNombre().equals("")){
            if(pointsLevel.matches("[0-9]+")){
                //Insertamos el Nivel
                level.setPuntaje(new BigDecimal(pointsLevel));
                servicio.edit(level);
                listaNiveles = servicio.findAllLevels();
                Collections.sort(listaNiveles, new Comparator<Niveles>(){
                    @Override
                    public int compare(Niveles o1, Niveles o2) {
                        return o1.getIdNiveles().compareTo(o2.getIdNiveles());
                    }
                });
                if(servicio.findById(listaNiveles.get(listaNiveles.size()-1).getIdNiveles()) != null){
                    action="/Private/Levels?faces-redirect=true";
                    FacesContext
                    .getCurrentInstance()
                    .addMessage("msg",new FacesMessage("Nivel editado!"));
                }else{
                    FacesContext.getCurrentInstance()
                        .addMessage("msg",new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Hubo un error la Base de Datos!",
                                "Hubo un error la Base de Datos!"));
                }
            }else{
                FacesContext.getCurrentInstance()
                            .addMessage("msg",new FacesMessage(
                                    FacesMessage.SEVERITY_ERROR,
                                    "El puntaje debe ser numerico!",
                                    "EL puntaje debe ser numerico!"));
            }
        }
        return action;
    }
    
    public void deleteLevel(Niveles levelAux){
        try {
            //servicio.remove(usuarioAux);
            levelAux.setBorrado(1);
            servicio.edit(levelAux);
            if(servicio.findById(levelAux.getIdNiveles()).getBorrado() == 1){
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Nivel Eliminado!"));
                levelTemp=new Niveles();
                level=new Niveles();
                listaNiveles = servicio.findAllLevels();
            }else{
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema al eliminar el nivel!"));
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema en la Base de Datos!"));
        }
    }
}
