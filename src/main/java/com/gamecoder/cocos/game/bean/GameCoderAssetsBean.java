/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.bean;

import com.gamecoder.cocos.game.entity.classes.Assets;
import com.gamecoder.cocos.game.entity.classes.TipoAsset;
import com.gamecoder.cocos.game.facades.AssetsFacadeLocal;
import com.gamecoder.cocos.game.facades.TipoAssetFacadeLocal;
import com.gamecoder.cocos.game.util.CommonsUtilsGameCoder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;

/**
 *
 * @author alan
 */
@ManagedBean
@ViewScoped
public class GameCoderAssetsBean implements Serializable{

    @EJB
    private AssetsFacadeLocal servicio;
    @EJB
    private TipoAssetFacadeLocal servTA;
    private List<Assets> listaAssets;
    private Assets assetTemp;
    private Assets asset;
    private Part file;
    private String fileContent;
    private String typeFile;
    private TipoAsset tipoAsset;
    
    /**
     * Creates a new instance of GameCoderAssetsBean
     */
    public GameCoderAssetsBean() {
    }
 
    @PostConstruct
    public void init(){
        try{
            listaAssets = servicio.findAllAssets();
            asset = new Assets();
            assetTemp = (Assets) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("assetEditar");
            
            String pagina=FacesContext.getCurrentInstance()
                        .getViewRoot()
                        .getViewId();
            if(assetTemp != null && !pagina.contains("AssetsInsert")){
                System.out.println("Entra en editar");
                System.out.println("Asset: "+assetTemp.getNombre());
                tipoAsset = servTA.findByAssetId(assetTemp.getIdAssets());
                System.out.println("Tipo Asset: "+tipoAsset.getIdTipoAsset());
            }else{
                 FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .remove("assetEditar");
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public List<Assets> getListaAssets() {
        return listaAssets;
    }

    public Assets getAssetTemp() {
        return assetTemp;
    }

    public void setAssetTemp(Assets assetTemp) {
        this.assetTemp = assetTemp;
    }

    public Assets getAsset() {
        return asset;
    }

    public void setAsset(Assets asset) {
        this.asset = asset;
    }

    public void setListaAssets(List<Assets> listaAssets) {
        this.listaAssets = listaAssets;
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }

    public String getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(String typeFile) {
        this.typeFile = typeFile;
    }

    public TipoAsset getTipoAsset() {
        return tipoAsset;
    }

    public void setTipoAsset(TipoAsset tipoAsset) {
        this.tipoAsset = tipoAsset;
    }
    
    public void InsertAssetViewRedirect(){
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Assets/AssetsInsert.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderAssetsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String createAssets(){
        String action = "";
        String path = this.getClass().getClassLoader().getResource("").getPath().replace("lib/classes/","Assets");
        System.out.println("Path: "+path);
        //Creamos la carpeta Assets en la instancia del dominio donde este el proyecto
        File file = new File(path);
        makeDirAssets(file);
        if(!asset.getNombre().equals("") && this.file != null
                && !typeFile.equals("")){
            //Creamos el Asset
            asset.setBorrado(0);
            String fileN = generateUniqueName(this.file.getSubmittedFileName())+ (this.file.getSubmittedFileName().contains(".png") ? ".png" : ".mp3");
            asset.setPath(path+File.separator+fileN);
            servicio.create(asset);
            listaAssets = servicio.findAllAssets();
            Collections.sort(listaAssets, new Comparator<Assets>(){
                @Override
                public int compare(Assets o1, Assets o2) {
                    return o1.getIdAssets().compareTo(o2.getIdAssets());
                }
            });
            Assets tmpAsset = listaAssets.get(listaAssets.size()-1);
            System.out.println("Asset ID: "+tmpAsset.getIdAssets());
            try {
                //Creamos el tipo de Asset
                TipoAsset asset = new TipoAsset();
                asset.setBorrado(0);
                asset.setTipo(typeFile);
                asset.setAssetsidAssets(tmpAsset);
                servTA.create(asset);
                //Creamos el Archivo
                uploadImage(tmpAsset.getPath(), this.file.getInputStream());
                //Redireccionamos
                action="/Private/Assets?faces-redirect=true";
            } catch (IOException ex) {
                Logger.getLogger(GameCoderAssetsBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            FacesContext
                .getCurrentInstance()
                .addMessage("msg",new FacesMessage("Datos no validos o vacios."));
        }
        return action;
    }
    
    public void validateFile(FacesContext ctx,UIComponent comp,Object value) {
        List<FacesMessage> msgs = new ArrayList<FacesMessage>();
        Part file = (Part)value;
        if(file != null){
            if (!"audio/mp3".equals(file.getContentType())
                    && !"image/png".equals(file.getContentType())) {
              msgs.add(new FacesMessage("Archivo no valido (mp3/png)"));
            }
            if (!msgs.isEmpty()) {
              throw new ValidatorException(msgs);
            }
        }
    }
    
    public boolean typeAssetRender(){
        return tipoAsset.getTipo().toLowerCase().equals("sprite");
    }
    
    public String updateAssets(){
        String action = "";
        
        String path = this.getClass().getClassLoader().getResource("").getPath().replace("lib/classes/","Assets");
        System.out.println("Path: "+path);
        //Creamos la carpeta Assets en la instancia del dominio donde este el proyecto
        File file = new File(path);
        makeDirAssets(file);
        if(this.file != null){
            //Creamos el Asset
            asset.setBorrado(0);
            String fileN = generateUniqueName(this.file.getSubmittedFileName())+ (this.file.getSubmittedFileName().contains(".png") ? ".png" : ".mp3");
            asset.setPath(path+File.separator+fileN);
            servicio.edit(asset);

            try {
                //Editamos el tipo de Asset
                tipoAsset.setTipo(typeFile);
                servTA.edit(tipoAsset);
                //Creamos el Archivo
                uploadImage(asset.getPath(), this.file.getInputStream());
                //Redireccionamos
                action="/Private/Assets?faces-redirect=true";
            } catch (IOException ex) {
                Logger.getLogger(GameCoderAssetsBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            servicio.edit(assetTemp);
            action="/Private/Assets?faces-redirect=true";
        }
        
        return action;
    }
    
    public void prepareForEdit(Assets asset){
        FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .put("assetEditar", asset);
        System.out.println("Entra en prepare edit");
        System.out.println("Asset select: "+asset.getNombre());
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Assets/AssetsEdit.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderAssetsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void makeDirAssets(File file){
        if(!file.exists()){
            file.mkdir();
        }
    }
    
    public void uploadImage(String file,InputStream entrada){
        try{
            OutputStream salida=new FileOutputStream(new File(file));
            int read=0;
            byte[] bytes=new byte[1024];
            while((read=entrada.read(bytes))!=-1){
                salida.write(bytes, 0, read);
            }
            entrada.close();
            salida.flush();
            salida.close();
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }
    
    public String generateUniqueName(String fileName){
        String date = new Date().getTime()+"";
        String diggest = "";
        diggest = CommonsUtilsGameCoder.sha256DigestHash(fileName+fastestRandomStringWithMixedCase(6)+date);
        return diggest;
    }
    
    public void deleteAsset(Assets assetAux){
        try {
            //servicio.remove(usuarioAux);
            assetAux.setBorrado(1);
            servicio.edit(assetAux);
            if(servicio.findById(assetAux.getIdAssets()).getBorrado() == 1){
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Asset Eliminado!"));
                assetTemp=new Assets();
                asset=new Assets();
                listaAssets = servicio.findAllAssets();
            }else{
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema al eliminar el asset!"));
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema en la Base de Datos!"));
        }
    }
    
    public String fastestRandomStringWithMixedCase(int length) {
        Random random = new Random();
        final int alphabetLength = 'Z' - 'A' + 1;
        StringBuilder result = new StringBuilder(length);
        while (result.length() < length) {
            final char charCaseBit = (char) (random.nextInt(2) << 5);
            result.append((char) (charCaseBit | ('A' + random.nextInt(alphabetLength))));
        }
        return result.toString();
    }
    
}
