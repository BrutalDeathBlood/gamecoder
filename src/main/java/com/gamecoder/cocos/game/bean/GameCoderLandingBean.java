/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.bean;

import com.gamecoder.cocos.game.entity.classes.Assets;
import com.gamecoder.cocos.game.facades.AssetsFacadeLocal;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author alan
 */
@ManagedBean
@ViewScoped
public class GameCoderLandingBean {

    @EJB
    private AssetsFacadeLocal servicio;
    private List<Assets> listaAssets;
    
    /**
     * Creates a new instance of GameCoderLandingBean
     */
    public GameCoderLandingBean() {
    }
    
    @PostConstruct
    public void init(){
       try{
           listaAssets = servicio.findAllAssets();
       } catch(Exception ex){
           ex.printStackTrace();
       }
    }

    public List<Assets> getListaAssets() {
        return listaAssets;
    }

    public void setListaAssets(List<Assets> listaAssets) {
        this.listaAssets = listaAssets;
    }
    
    public StreamedContent displayImages(String file){
        File arch=null;
        arch=new File(file);
        StreamedContent stream = null;
        if(arch.exists()){
            try {
                stream= new DefaultStreamedContent(new FileInputStream(arch),"image/png");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GameCoderLandingBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return stream;
    }
}
