/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.bean;

import com.gamecoder.cocos.game.entity.classes.Usuarios;
import com.gamecoder.cocos.game.facades.UsuariosFacadeLocal;
import com.gamecoder.cocos.game.util.CommonsUtilsGameCoder;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author alan
 */
@ManagedBean
@ViewScoped
public class GameCoderUsuariosBean implements Serializable{
    
    private Usuarios usuario;
    @EJB
    private UsuariosFacadeLocal servicio;
    private List<Usuarios> listaUsuarios;
    private String confirmPass;
    private String previousPass;
    private Usuarios usuarioTmp;
    private Usuarios usuarioEdit;

    /**
     * Creates a new instance of GameCoderUsuariosBean
     */
    public GameCoderUsuariosBean() {
    }
    
    @PostConstruct
    public void inicio(){
        try {
            usuario=(Usuarios) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("usuario");
            listaUsuarios = servicio.findUsers();
            
            usuarioEdit = (Usuarios) FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .get("usuarioEditar");
            
            usuarioTmp = new Usuarios();
            String pagina=FacesContext.getCurrentInstance()
                        .getViewRoot()
                        .getViewId();
            if(usuarioEdit != null && !pagina.contains("UsuarioInsert")){
                System.out.println("Entra en editar");
                System.out.println("User: "+usuarioEdit.getCorreo());
                previousPass = usuarioEdit.getPassword();
                confirmPass = usuarioEdit.getPassword();
            }else{
                 FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .remove("usuarioEditar");
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("mensaje", new FacesMessage("Error en la db"));
        }
    }

    public Usuarios getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuarios usuario) {
        this.usuario = usuario;
    }

    public List<Usuarios> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuarios> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }

    public Usuarios getUsuarioTmp() {
        return usuarioTmp;
    }

    public void setUsuarioTmp(Usuarios usuarioTmp) {
        this.usuarioTmp = usuarioTmp;
    }

    public Usuarios getUsuarioEdit() {
        return usuarioEdit;
    }

    public void setUsuarioEdit(Usuarios usuarioEdit) {
        this.usuarioEdit = usuarioEdit;
    }

    public String getPreviousPass() {
        return previousPass;
    }

    public void setPreviousPass(String previousPass) {
        this.previousPass = previousPass;
    }

    public String constructName(){
        if(usuario != null){
            return usuario.getNombre()+" "+usuario.getPaterno()+" "+usuario.getMaterno();
        }
        return "";
    }
    
    /**
     * Metodo que redirecciona a la vista de Create del CRUD de usuarios
     */
    public void InsertUserView(){
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Usuarios/UsuarioInsert.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderUsuariosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * Metodo para insertar un usuario 
     * en la base de datos (Create)
     * 
     * Se valida que el usuario no exista
     * 
     * @return url destino si tuvo exito, en caso
     * contrario cadena vacia
     */
    public String createUser(){
        String action="";
        if(usuarioTmp.getPassword().equals(confirmPass)){
            //Obtenemos Hash de  password
            usuarioTmp.setPassword(CommonsUtilsGameCoder.sha256DigestHash(confirmPass));
            usuarioTmp.setBorrado(0);
            try{
                if(servicio.findByUser(usuarioTmp) == null){
                    //Creamos usuario
                    servicio.create(usuarioTmp);
                    
                    //Buscamos el usuario agregado
                    if(servicio.findByUser(usuario) != null){
                        action="/Private/Usuarios?faces-redirect=true";
                        FacesContext
                        .getCurrentInstance()
                        .addMessage("msg",new FacesMessage("Usuario guardado!"));
                    }else{
                        FacesContext.getCurrentInstance()
                            .addMessage("msg",new FacesMessage(
                                    FacesMessage.SEVERITY_ERROR,
                                    "Hubo un error al insertar!",
                                    "Hubo un error al insertar!"));
                    }
                }else{
                    //Si el usuario existe vemos si esta borrado
                    Usuarios uTemp=servicio.findByUser(usuario);
                    if(uTemp!=null){
                        if(uTemp.getBorrado() == 1){
                            usuarioTmp.setIdUsuarios(uTemp.getIdUsuarios());
                            servicio.edit(uTemp);
                            action="/Private/Usuarios?faces-redirect=true";
                        }else{
                            FacesContext.getCurrentInstance()
                            .addMessage("msg",new FacesMessage(
                                    FacesMessage.SEVERITY_ERROR,
                                    "Login no disponible!",
                                    "Login no disponible!"));
                        }
                    }else{
                        FacesContext.getCurrentInstance()
                            .addMessage("msg",new FacesMessage(
                                    FacesMessage.SEVERITY_ERROR,
                                    "Correo no disponible!",
                                    "Correo no disponible!"));
                    }
                }
            }catch(Exception ex){
                ex.printStackTrace();
                FacesContext.getCurrentInstance()
                        .addMessage("msg",new FacesMessage(
                                FacesMessage.SEVERITY_ERROR,
                                "Hubo un error la Base de Datos!",
                                "Hubo un error la Base de Datos!"));
            }
        }else{
            FacesContext.getCurrentInstance()
                    .addMessage("msg",new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "Las contraseñas deben coincidir!", 
                            "Las contraseñas deben coincidir!"));
        }
        
        return action;
    }
    
    public void prepareForEdit(Usuarios user){
        FacesContext.getCurrentInstance()
                        .getExternalContext()
                        .getSessionMap()
                        .put("usuarioEditar", user);
        System.out.println("Entra en prepare edit");
        System.out.println("Usuario select: "+user.getCorreo());
        try {
            FacesContext contexto=FacesContext.getCurrentInstance();
            contexto.getExternalContext().redirect("./Usuarios/UsuarioEdit.gamecoder");
        } catch (IOException ex) {
            Logger.getLogger(GameCoderUsuariosBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * Metodo que edita un usuario
     * verifica si las contrasenias son diferentes
     * si no lo son solo edita la informacion del perfil
     * 
     * @return 
     */
    public String editUser(){
        String accion="";
        int error=0;
        System.out.println("Entra en actualizar.");
        //checamos si hubo un cambio de contraseña, 
        //de ser asi actualizamos el valor de la misma
        
        if(!previousPass.equals(CommonsUtilsGameCoder.sha256DigestHash(confirmPass))
                && !confirmPass.equals("")){
            if(usuarioEdit.getPassword().equals(confirmPass)){
                usuarioEdit.setPassword(CommonsUtilsGameCoder.sha256DigestHash(confirmPass));
            }else{
                error=1;
                FacesContext
                    .getCurrentInstance()
                    .addMessage("msg",new FacesMessage("Las contraseñas deben de coincidir!"));
            }
        }
        //verificamos que no exista un usuario con ese usuario y correo electronico
        Usuarios auxiliar;
        try {
            auxiliar = servicio.findByUser(usuarioEdit);
            if(auxiliar!=null){
                if(!auxiliar.getIdUsuarios().equals(usuarioEdit.getIdUsuarios())){
                    FacesContext
                        .getCurrentInstance()
                        .addMessage("msg",new FacesMessage("Login o Correo no disponibles!"));
                    error=1;
                }
            }
        } catch (Exception ex) {
            FacesContext
                .getCurrentInstance()
                .addMessage("msg",new FacesMessage("Error en la Base de Datos!"));
        }
        //si no hubo ningun error en las validaciones, procedemos a actualizar el usuario
        if(error==0){
            try {
                servicio.edit(usuarioEdit);
                accion = "/Private/Usuarios?faces-redirect=true";
                FacesContext
                        .getCurrentInstance()
                        .addMessage("msg",new FacesMessage("Usuario actualizado!"));
            } catch (Exception ex) {
                ex.printStackTrace();
                FacesContext
                    .getCurrentInstance()
                    .addMessage("msg",new FacesMessage("Error en la Base de Datos!"));
            }
        }
        return accion;
    }
    
    /**
     * Metodo que sirve para dar de baja un usuario
     * @param usuarioAux 
     */
    public void deleteUser(Usuarios usuarioAux){
        try {
            //servicio.remove(usuarioAux);
            usuarioAux.setBorrado(1);
            servicio.edit(usuarioAux);
            if(servicio.findByUser(usuarioAux).getBorrado() == 1){
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Usuario Eliminado!"));
                usuario=new Usuarios();
                usuarioEdit=new Usuarios();
                listaUsuarios = servicio.findUsers();
            }else{
                FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema al eliminar el usuario!"));
            }
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage("msg",new FacesMessage("Hubo un problema en la Base de Datos!"));
        }
    }
    
    public String constructNameByUser(Usuarios usuario){
        if(usuario != null){
            return usuario.getNombre()+" "+usuario.getPaterno()+" "+usuario.getMaterno();
        }
        return "";
    }
}
