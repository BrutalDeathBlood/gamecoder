/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.converter;

import com.gamecoder.cocos.game.entity.classes.Niveles;
import com.gamecoder.cocos.game.facades.NivelesFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author alan
 */
@FacesConverter("convertidorLevels")
public class LevelsConverter implements Converter{

    @EJB
    private NivelesFacadeLocal servicio;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println("Value: "+value);
        if(value != null && value.trim().length() > 0) {
            try {
                Niveles level=null;
                List<Niveles> lista = new ArrayList<>();
                lista = servicio.findAllLevels();
                System.out.println("SValue: "+value);
                for (Niveles obj : lista) {
                    if(obj.getIdNiveles()==Integer.parseInt(value)){
                        level=obj;
                        break;
                    }
                }
                return level;
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nivel no valido.", "Nivel no valido."));
            }
        }
        else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            System.out.println("getAsString-->null");
            return null;
        } else {
            //System.out.println("getAsString-->"+((RolesPojo)value).getIdRol());
            return String.valueOf(((Niveles)value).getIdNiveles());
        }
    }
    
}
