/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.converter;

import com.gamecoder.cocos.game.entity.classes.Assets;
import com.gamecoder.cocos.game.facades.AssetsFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author alan
 */
@FacesConverter("convertidorAssets")
public class AssetsConverter implements Converter{

    @EJB
    private AssetsFacadeLocal servicio;
    
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        System.out.println("Value: "+value);
        if(value != null && value.trim().length() > 0) {
            try {
                Assets asset=null;
                List<Assets> lista = new ArrayList<>();
                lista = servicio.findAllAssets();
                System.out.println("SValue: "+value);
                for (Assets obj : lista) {
                    if(obj.getIdAssets()==Integer.parseInt(value)){
                        asset=obj;
                        break;
                    }
                }
                return asset;
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Asset no valido.", "Asset no valido."));
            }
        }
        else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null || value.equals("")) {
            System.out.println("getAsString-->null");
            return null;
        } else {
            //System.out.println("getAsString-->"+((RolesPojo)value).getIdRol());
            return String.valueOf(((Assets)value).getIdAssets());
        }
    }
    
}
