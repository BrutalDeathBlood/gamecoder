/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.TipoAsset;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author alan
 */
@Local
public interface TipoAssetFacadeLocal {

    void create(TipoAsset tipoAsset);

    void edit(TipoAsset tipoAsset);

    void remove(TipoAsset tipoAsset);

    TipoAsset find(Object id);

    List<TipoAsset> findAll();

    List<TipoAsset> findRange(int[] range);

    int count();
    
    TipoAsset findByAssetId(int id);
    
}
