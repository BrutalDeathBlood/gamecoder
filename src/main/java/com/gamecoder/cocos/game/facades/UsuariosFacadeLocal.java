/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Usuarios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author alan
 */
@Local
public interface UsuariosFacadeLocal {

    void create(Usuarios usuarios);

    void edit(Usuarios usuarios);

    void remove(Usuarios usuarios);

    Usuarios find(Object id);

    List<Usuarios> findAll();

    List<Usuarios> findRange(int[] range);

    int count();
    
    Usuarios inicioSesion (Usuarios usuario);
    
    List<Usuarios> findUsers();
    
    Usuarios findByUser(Usuarios usuario);
    
}
