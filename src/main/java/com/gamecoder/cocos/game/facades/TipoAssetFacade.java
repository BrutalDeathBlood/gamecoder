/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.TipoAsset;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author alan
 */
@Stateless
public class TipoAssetFacade extends AbstractFacade<TipoAsset> implements TipoAssetFacadeLocal {

    @PersistenceContext(unitName = "com.gamecoder.cocos_GameCoder_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoAssetFacade() {
        super(TipoAsset.class);
    }

    @Override
    public TipoAsset findByAssetId(int id) {
        TipoAsset tAsset=null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("TipoAsset.findByAssetId");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,0);
            query.setParameter(2,id);
            List<TipoAsset> tAssets=query.getResultList();
            if(!tAssets.isEmpty()){
                tAsset=tAssets.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return tAsset;
    }
    
}
