/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author alan
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> implements UsuariosFacadeLocal {

    @PersistenceContext(unitName = "com.gamecoder.cocos_GameCoder_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }

    @Override
    public Usuarios inicioSesion(Usuarios usuario) {
        Usuarios usuarioFinal=null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Usuarios.login");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,usuario.getCorreo());
            query.setParameter(2,usuario.getPassword());
            List<Usuarios> usuarios=query.getResultList();
            if(!usuarios.isEmpty()){
                usuarioFinal=usuarios.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return usuarioFinal;
    }

    @Override
    public List<Usuarios> findUsers() {
        List<Usuarios> listUsers = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Usuarios.findUsers");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,0);
            List<Usuarios> usuarios=query.getResultList();
            if(!usuarios.isEmpty()){
                listUsers = usuarios;
            }
        } catch (Exception e) {
            throw e;
        }
        return listUsers;
    }

    @Override
    public Usuarios findByUser(Usuarios usuario) {
        Usuarios usuarioFinal=null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Usuarios.findByUser");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,usuario.getCorreo());
            query.setParameter(2,usuario.getNombre());
            query.setParameter(3,usuario.getMaterno());
            query.setParameter(4,usuario.getPaterno());
            List<Usuarios> usuarios=query.getResultList();
            if(!usuarios.isEmpty()){
                usuarioFinal=usuarios.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return usuarioFinal;
    }
    
}
