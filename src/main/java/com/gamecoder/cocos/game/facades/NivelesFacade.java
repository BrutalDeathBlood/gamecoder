/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Niveles;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author alan
 */
@Stateless
public class NivelesFacade extends AbstractFacade<Niveles> implements NivelesFacadeLocal {

    @PersistenceContext(unitName = "com.gamecoder.cocos_GameCoder_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public NivelesFacade() {
        super(Niveles.class);
    }

    @Override
    public List<Niveles> findAllLevels() {
         List<Niveles> listLevels = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Niveles.findAllLevels");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,0);
            List<Niveles> levels=query.getResultList();
            if(!levels.isEmpty()){
                listLevels = levels;
            }
        } catch (Exception e) {
            throw e;
        }
        return listLevels;
    }

    @Override
    public Niveles findById(int id) {
        Niveles level = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Niveles.findById");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1, id);
            List<Niveles> levels=query.getResultList();
            if(!levels.isEmpty()){
                level = levels.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return level;
    }
    
}
