/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Niveles;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author alan
 */
@Local
public interface NivelesFacadeLocal {

    void create(Niveles niveles);

    void edit(Niveles niveles);

    void remove(Niveles niveles);

    Niveles find(Object id);

    List<Niveles> findAll();

    List<Niveles> findRange(int[] range);

    int count();
    
    List<Niveles> findAllLevels();
    
    Niveles findById(int id);
}
