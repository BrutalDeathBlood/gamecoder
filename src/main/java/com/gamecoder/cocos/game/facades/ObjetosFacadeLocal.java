/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Objetos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author alan
 */
@Local
public interface ObjetosFacadeLocal {

    void create(Objetos objetos);

    void edit(Objetos objetos);

    void remove(Objetos objetos);

    Objetos find(Object id);

    List<Objetos> findAll();

    List<Objetos> findRange(int[] range);

    int count();
    
    List<Objetos> findAllObjects();
    
    Objetos findById(int id);
    
}
