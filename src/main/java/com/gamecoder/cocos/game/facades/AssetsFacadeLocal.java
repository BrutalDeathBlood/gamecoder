/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Assets;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author alan
 */
@Local
public interface AssetsFacadeLocal {

    void create(Assets assets);

    void edit(Assets assets);

    void remove(Assets assets);

    Assets find(Object id);

    List<Assets> findAll();

    List<Assets> findRange(int[] range);

    int count();
    
    List<Assets> findAllAssets();
    
    Assets findAssetById(int id);
    
    Assets findById(int id);
    
}
