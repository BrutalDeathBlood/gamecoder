/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Assets;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author alan
 */
@Stateless
public class AssetsFacade extends AbstractFacade<Assets> implements AssetsFacadeLocal {

    @PersistenceContext(unitName = "com.gamecoder.cocos_GameCoder_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AssetsFacade() {
        super(Assets.class);
    }

    @Override
    public List<Assets> findAllAssets() {
        List<Assets> listAssets = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Assets.findAllAssets");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1, 0);
            List<Assets> assets=query.getResultList();
            if(!assets.isEmpty()){
                listAssets = assets;
            }
        } catch (Exception e) {
            throw e;
        }
        return listAssets;
    }

    @Override
    public Assets findAssetById(int id) {
        Assets asset = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Assets.findById");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1, 0);
            query.setParameter(2, id);
            List<Assets> assets=query.getResultList();
            if(!assets.isEmpty()){
                asset = assets.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return asset;
    }

    @Override
    public Assets findById(int id) {
        Assets asset = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Assets.findByIdAll");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1, id);
            List<Assets> assets=query.getResultList();
            if(!assets.isEmpty()){
                asset = assets.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return asset;
    }
    
}
