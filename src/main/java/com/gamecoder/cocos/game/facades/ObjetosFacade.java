/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamecoder.cocos.game.facades;

import com.gamecoder.cocos.game.entity.classes.Objetos;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author alan
 */
@Stateless
public class ObjetosFacade extends AbstractFacade<Objetos> implements ObjetosFacadeLocal {

    @PersistenceContext(unitName = "com.gamecoder.cocos_GameCoder_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ObjetosFacade() {
        super(Objetos.class);
    }

    @Override
    public List<Objetos> findAllObjects() {
        List<Objetos> listObjetcs = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Objetos.findAllObjects");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1,0);
            List<Objetos> objects=query.getResultList();
            if(!objects.isEmpty()){
                listObjetcs = objects;
            }
        } catch (Exception e) {
            throw e;
        }
        return listObjetcs;
    }

    @Override
    public Objetos findById(int id) {
        Objetos object = null;
        try {
            //em.getEntityManagerFactory().getCache().evictAll();
            Query query=em.createNamedQuery("Objetos.findById");
            query.setHint("eclipselink.refresh", "true");
            query.setParameter(1, id);
            List<Objetos> levels=query.getResultList();
            if(!levels.isEmpty()){
                object = levels.get(0);
            }
        } catch (Exception e) {
            throw e;
        }
        return object;
    }
    
}
